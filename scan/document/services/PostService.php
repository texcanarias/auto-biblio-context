<?php
declare(strict_types=1); // strict mode
namespace scan\document\services;

use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\document\messages\PostMessage;

class PostService{
    public static function execute( PostMessage $message, 
                                    InterfacePersistenceDocument $per) : Document{
        try{
            try{
                $document = $message->getDocument();
                $newId = $per->saveDataToPersistenceSystem($document);
                $document->setId($newId);
            } catch(\Exception $ex){
                echo $ex->getMessage();
            }
        }
        catch(\Exception $ex){
            throw $ex;
        }

        return $document;
    }
}