<?php
declare(strict_types=1); // strict mode
namespace scan\document\services;

use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\document\messages\DeleteMessage;

class DeleteService{

    public static function execute( DeleteMessage $message, 
                                    InterfacePersistenceDocument $per) : Document{
        try{
            $document = $per->deleteDataFromPersistenceSystem($message->getDocumentId());
        }
        catch(\Exception $ex){
            throw $ex;
        }

        return $document;
    }
}