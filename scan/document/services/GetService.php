<?php
declare(strict_types=1); // strict mode
namespace scan\document\services;

use scan\document\models\ArrayDocument;
use scan\document\persistences\InterfacePersistenceArrayDocument;

class GetService{
    public static function execute(InterfacePersistenceArrayDocument $per) : ArrayDocument{
        try{
            $documents = $per->getAll();
        }
        catch(\Exception $ex){
            throw $ex;
        }

        return $documents;
    }
}