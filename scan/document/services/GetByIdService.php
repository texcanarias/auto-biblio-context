<?php
declare(strict_types=1); // strict mode
namespace scan\document\services;

use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\document\messages\GetByIdMessage;

class GetByIdService{
    public static function execute( GetByIdMessage $message, 
                                    InterfacePersistenceDocument $per) : Document{
        try{
            $document = $per->getDataFromPersistenceSystem($message->getDocumentId());
        }
        catch(\Exception $ex){
            throw $ex;
        }

        return $document;
    }
}