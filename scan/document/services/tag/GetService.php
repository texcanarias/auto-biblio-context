<?php
declare(strict_types=1); // strict mode
namespace scan\document\services\tag;

use scan\document\models\ArrayDocument;
use scan\document\persistences\InterfacePersistenceArrayDocument;
use scan\document\messages\GetMessagesByTag;

class GetService{
    public static function execute( GetMessagesByTag $message , 
                                    InterfacePersistenceArrayDocument $per) : ArrayDocument{
        try{
          $documents = $per->getAllFromTag($message->getTag());
        }
        catch(\Exception $ex){
            throw $ex;
        }

        return $documents;
    }
}