<?php
declare(strict_types=1); // strict mode
namespace scan\document\messages;

class DeleteMessage{
    private int $documentId;

    private function __construct(int $documentId){
        $this->documentId = $documentId; 
    }

    public static function create(int $documentId) : self{
        $isNumber = is_numeric($documentId) && 0 < $documentId;
        if(!$isNumber){
            throw new \Exception('Debe ser un numero positivo');
        }

        return new self($documentId);
    }

    public function getDocumentId() : int{
        return $this->documentId;
    }
}