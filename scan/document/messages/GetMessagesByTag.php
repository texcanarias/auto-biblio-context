<?php
declare(strict_types=1); // strict mode
namespace scan\document\messages;

class GetMessagesByTag{
    private string $tag;

    private function __construct(string $tag){
        $this->tag = $tag; 
    }

    public static function create(string $tag) : self{
        return new self($tag);
    }

    public function getTag() : string{
        return $this->tag;
    }
}