<?php
declare(strict_types=1); // strict mode
namespace scan\document\messages;

use scan\document\models\Document;

class PostMessage{
    private Document $item;

    private function __construct(Document $item){
        $this->item = $item; 
    }

    public static function create(Document $item) : self{
        return new self($item);
    }

    public function getDocument() : Document{
        return $this->item;
    }
}