# Modelos

TODO: Terminar de implementar las funciones descritas en <https://gist.github.com/Jeff-Russ/e1f64273a471d440e8b4d9183f9a2667>

```plantuml
@startuml

class Tag{
    - ?int $id
    - string $name;
    ---
    - __construct(?int $id, string $name)
    + <<static>> factoryNew(?int $id, string $name) : self
    + getId() : int
    + setId(int $id)
    + getName() : string
}

class Document{
    - ?int $id;
    - string $name;
    - string $nameFile;
    - string $mime;
    - ArrayTag $tags;
    ---
    - __construct(?int $id,
                                string $name,
                                string $nameFile,
                                string $mime,
                                ArrayTag $tags)

    + <<static>> factoryFromArray(?int $id,
                                            string $name,
                                            string $nameFile,
                                            string $mime,
                                            array $tags) : self

    + <<static>> factoryFromArrayTag( ?int $id,
                                                string $name,
                                                string $nameFile,
                                                string $mime,
                                                ArrayTag $arrayTags) : self

    + getId() : ?int
    + setId(int $id) : self
    + getName() : string
    + getNameFile() : string
    + getMime() : string
    + getTags() : ArrayTag
    + setTag(Tag $tag)
}

Document -- Tag
Document *-- ArrayTag

interface IteratorAggregate
interface Countable

class ArrayTag  {
    protected array $listado;
    protected int $total;
    ---

    + __construct()
    + getListado() : array
    + setListado(array $listado)
    + getTotal() : int
    + setTotal(int $Total)
    + getTotalParcial() : int
    + count() : int
    + isVacio() : bool
    + getIterator() : \ArrayIterator
    + add(Tag $value)
}

ArrayTag -- Tag

ArrayTag <|-- IteratorAggregate
ArrayTag <|-- Countable

class ArrayDocument  {
    protected array $listado;
    protected int $total;
    ---

    + __construct()
    + getListado() : array
    + setListado(array $listado)
    + getTotal() : int
    + setTotal(int $Total)
    + getTotalParcial() : int
    + count() : int
    + isVacio() : bool
    + getIterator() : \ArrayIterator
    + add(Tag $value)
}

ArrayDocument <|-- IteratorAggregate
ArrayDocument <|-- Countable
ArrayDocument -- Array
@enduml
```
