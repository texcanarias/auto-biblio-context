<?php
declare(strict_types=1); // strict mode
namespace scan\document\models;

class Tag implements \JsonSerializable {
    private ?int $id;
    private string $name;

    private function __construct(?int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    static public function factoryNew(?int $id, string $name) : self{
        $new = new self($id, $name);
        return $new;
    }

    public function getId() : int{
        return $this->id;
    } 

    public function setId(int $id){
        $this->id = $id;
        return $this;
    }

    public function getName() : string{
        return $this->name;
    }

    public function jsonSerialize() : array{
        return ["id" => $this->id , "name" => $this->name];
    }

}