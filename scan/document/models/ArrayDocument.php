<?php
declare(strict_types=1); // strict mode
namespace scan\document\models;

class ArrayDocument extends ArrayPrimitive{    
   /**
     * Añadimos un elemento a la lista
     * Modificamos el total
     * @param Document $value
     */
    public function add(Document $value) {
        $this->listado[] = $value;
        $this->total = count($this->listado);
    }    


    public function offsetSet($index, $value){
        if('scan\document\models\Document' == get_class($value)){
            return parent::offsetSet($index, $value);
        }
        else{
            throw new ExceptionArrayDocumentNoClass();
        }
    }


}