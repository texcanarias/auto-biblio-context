<?php
declare(strict_types=1); // strict mode
namespace scan\document\models;

class Document implements \JsonSerializable {
    private ?int $id;
    private string $name;
    private string $nameFile;
    private string $mime;
    private ArrayTag $tags;

    private function __construct(?int $id,
                                string $name, 
                                string $nameFile,
                                string $mime,
                                ArrayTag $tags)
    {
        $this->id = $id;
        $this->name = $name;
        $this->nameFile = $nameFile;
        $this->mime = $mime;
        $this->tags = $tags;
    }

    static public function factoryFromArray(?int $id,
                                            string $name, 
                                            string $nameFile,
                                            string $mime,
                                            array $tags) : self{
        $arrayTags = new ArrayTag();
        foreach($tags as $tag){
            $arrayTags->add(Tag::factoryNew(null, $tag));
        }
        return new self($id, $name, $nameFile, $mime, $arrayTags);
    }

    static public function factoryFromArrayTag( ?int $id,
                                                string $name, 
                                                string $nameFile,
                                                string $mime,
                                                ArrayTag $arrayTags) : self{
        return new self($id, $name, $nameFile, $mime, $arrayTags);
    }

    function getId() : ?int{
        return $this->id;
    }

    function setId(int $id) : self{
        $this->id = $id;
        return $this;
    }

    function getName() : string{
        return $this->name;
    }

    function getNameFile() : string{
        return $this->nameFile;
    }

    function getMime() : string{
        return $this->mime;
    }

    function getTags() : ArrayTag{
        return $this->tags;
    }

    function setTag(Tag $tag){
        $this->tags->add($tag);
    }

    /**
     * Implementacion de la interfaz JsonSerializable
     * @return array
     */
    public function jsonSerialize() : array{
        return [    "id" => $this->id,
                    "name" => $this->name,
                    "name_file" => $this->nameFile,
                    "mime" => $this->mime,
                    "tags" => $this->tags->jsonSerialize() ];
    }
}
