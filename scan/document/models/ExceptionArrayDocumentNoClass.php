<?php
declare(strict_types=1); // strict mode
namespace scan\document\models;

class ExceptionArrayDocumentNoClass extends \Exception{
    public function __construct() {
        parent::__construct('Data must be a Document instance');
    }
} 