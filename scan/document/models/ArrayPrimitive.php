<?php
declare(strict_types=1); // strict mode
namespace scan\document\models;

use ArrayIterator;

class ArrayPrimitive implements \IteratorAggregate , \Countable , \ArrayAccess, \JsonSerializable{
    /**
     * @var array Vector lon los objetos devueltos
     */
    protected array $listado;
    
    /**
     * @var int Total de los registros en el sistema de pesistencia
     */
    protected int $total;
    
    public function __construct() {
        $this->listado = array();
        $this->total = 0;
    }
    
    public function getListado() : array {
        return $this->listado;
    }

    public function setListado(array $listado) {
        $this->listado = $listado;
        return $this;
    }

    public function getTotal() : int{
        return $this->total;
    }

    public function setTotal(int $Total) {
        $this->total = $Total;
        return $this;
    }
    
    public function getTotalParcial() : int {
        return count($this->listado);
    }

    
    /**
     * Implementa el método count
     * @return int
     */
    function count() : int{
        return count($this->listado);
    }
    
    
    /**
     * Si no hay elementos devuelve 0 en caso contrario 1
     * @return bool Si no hay elementos devuelve 1 en caso contrario 0;
     */
    public function isVacio() : bool{
        return 0 == $this->total;
    }
    
    /**
     * Se requiere la definición de la interfaz IteratorAggregate
     * @return MyIterator 
     */
    public function getIterator() : ArrayIterator {
        return new ArrayIterator($this->listado);
    }

    /**
     * Implementacion de la interfaz JsonSerializable
     * @return array
     */
    public function jsonSerialize() : array{
        $res = [];
        foreach($this->listado as $item){
            $res[] = $item->jsonSerialize();
        }
        return $res;
    }    

    public function offsetExists($index) : bool{
        return isset($this->listado[$index]);
    }

    public function offsetGet($index) {
        if($this->offsetExists($index)){
            return $this->listado[$index];
        } else {
            return false;
        }
    }
    public function offsetSet($index, $value) {
        if($index): $this->listado[$index] = $value;
        else: $this->listado[] = $value; endif;
        return true;
    }
    
    public function offsetUnset($index) : bool {
        unset($this->listado[$index]);
        return true;
    }
}