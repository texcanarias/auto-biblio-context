<?php
declare(strict_types=1); // strict mode
namespace scan\document\persistences;

class AtomFilter {
    private $min;
    private $max;
    private bool $exact;

    private function __construct($min, $max, bool $exact = false){
        $this->min = $min;
        $this->max = $max;
        $this->exact = $exact;
    }

    public function factory($min, $max, bool $exact = false) : self{
        return self::__construct($min, $max, $exact);
    }

    public function getMin(){
        return $this->min;
    }

    public function getMax(){
        return $this->max;
    }

    public function getExact(){
        return $this->Exact;
    }
}