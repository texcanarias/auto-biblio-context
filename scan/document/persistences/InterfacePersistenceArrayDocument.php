<?php
declare(strict_types=1); // strict mode
namespace scan\document\persistences;

use \scan\document\models\ArrayDocument;

interface InterfacePersistenceArrayDocument
{
    /**
     * Recuperacion de todos los registros
     */
    public function getAll() : ArrayDocument;

    /**
     * Recuperacion de todos los registros según un criterio de tener un tag determinado
     */
    public function getAllFromTag(string $tag) : ArrayDocument;

    /**
     * Determinar la paginación
     */
    public function setPagination(int $page, int $num) : void;

    /**
     * Deterninar filtros de busqueda
     */
    public function setFilter(DocumentFilter $documentFilter) : void;
}