<?php
declare(strict_types=1); // strict mode
namespace scan\document\persistences;

use \scan\document\models\Document;

interface InterfacePersistenceDocument
{
    public function saveDataToPersistenceSystem(Document $document) : int;

    public function getDataFromPersistenceSystem(int $id) : ?Document;

    public function deleteDataFromPersistenceSystem(int $id) : Document;
}