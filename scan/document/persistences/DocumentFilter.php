<?php
declare(strict_types=1); // strict mode
namespace scan\document\persistences;

class DocumentFilter {
    private AtomFilter $id;
    private AtomFilter $name;
    private AtomFilter $nameFile;
    private AtomFilter $mime;

    private function __construct(AtomFilter $id,
                                AtomFilter $name, 
                                AtomFilter $nameFile,
                                AtomFilter $mime)
    {
        $this->id = $id;
        $this->name = $name;
        $this->nameFile = $nameFile;
        $this->mime = $mime;
    }

    static public function factory( AtomFilter $id,
                                    AtomFilter $name, 
                                    AtomFilter $nameFile,
                                    AtomFilter $mime) : self{
        return new self($id, $name, $nameFile, $mime, $arrayTags);
    }

    function getId() : AtomFilter{
        return $this->id;
    }

    function getName() : AtomFilter{
        return $this->name;
    }

    function getNameFile() : AtomFilter{
        return $this->nameFile;
    }

    function getMime() : AtomFilter{
        return $this->mime;
    }

    function getTags() : AtomFilter{
        return $this->tags;
    }
}
