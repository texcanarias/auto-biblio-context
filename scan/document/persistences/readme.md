# Persistences

```plantuml
@startuml

interface InterfacePersistenceArrayDocument
{
    + getAll() : ArrayDocument
    + getAllFromTag(string $tag) : ArrayDocument;
}    

interface InterfacePersistenceDocument
{
    + <<static>> factoryPersistenceDocument() : self;
    + saveDataToPersistenceSystem(Document $document) : int;
    +getDataFromPersistenceSystem(int $id) : ?Document;
    + deleteDataFromPersistenceSystem(int $id) : Document;
}

@enduml
```