<?php
declare(strict_types=1); // strict mode

namespace scan\services\files;

class Files implements InterfaceFiles
{
    private string $file;
    private string $extension;
    private string $mime;
    /**
     * @var array
     */
    private array $tags;
    private string $newUniqueName;

    private function __construct(string $file)
    {
        $this->file = $file;
        $this->setExtension();
        $this->setMimeType();
        $this->setNewUniqueName();
        $this->setTags();
    }

    public static function buildFiles(string $file): self
    {
        return new self($file);
    }

    /**
     * Obtiene la extensión de un fichero
     * @param string $file Nombre del fichero
     * @return string extension
     */
    private function setExtension(): self
    {
        $idx = explode('.', $this->file);
        $count_explode = count($idx);
        $this->extension = strtolower($idx[$count_explode - 1]);
        return $this;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * Recupera el mime en base a la extensión
     * @param string $extension
     * @return string Mime's name
     */
    private function setMimeType(): self
    {
        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

// images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

// archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

// audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

// adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

// ms office
            'test' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',

// open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $isExistExtension = isset($mimet[$this->extension]);
        $this->mime = ($isExistExtension) ? $mimet[$this->extension] : 'application/octet-stream';
        return $this;
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * Obtener tags en base al nombre del fichero
     * @param string $name File's name
     * @return self
     */
    private function setTags(): self
    {
        $Vname = explode('.', $this->file);
        $count_explode = count($Vname);
        unset($Vname[$count_explode - 1]);
        $name = implode("", $Vname);
        $name = str_replace("_", " ", $name);
        $name = str_replace(",", " ", $name);
        $name = str_replace("-", " ", $name);
        $name = str_replace(".", " ", $name);
        $name = str_replace(";", " ", $name);
        $this->tags = explode(" ", $name);
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * Crear un nombre único de fichero con su extension
     * @param string File's extension
     * @return self
     */
    private function setNewUniqueName(): self
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $head = substr(str_shuffle($permitted_chars), 0, 4);

        $uniqueId = uniqid();
        $this->newUniqueName = $head . "-" . $uniqueId . "." . $this->extension;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewUniqueName(): string
    {
        return $this->newUniqueName;
    }


}