<?php
declare (strict_types = 1); // strict mode

namespace scan\services\files;

interface InterfaceFiles
{
    public function getExtension(): string;

    /**
     * @return string
     */
    public function getMime(): string;

    /**
     * @return array
     */
    public function getTags(): array;

    /**
     * @return string
     */
    public function getNewUniqueName(): string;
}