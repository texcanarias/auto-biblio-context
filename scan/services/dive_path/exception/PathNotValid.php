<?php
declare (strict_types = 1); // strict mode

namespace scan\services\dive_path\exception;

use Throwable;

class PathNotValid extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}