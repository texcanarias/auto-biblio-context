<?php

declare (strict_types = 1); // strict mode

namespace scan\services\dive_path;

use scan\services\dive_path\exception\PathNotValid;
use stdClass;
use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\services\files\Files;

class DivePath
{
    protected array $PathFiles;
    private InterfacePersistenceDocument $PersistenceDocument;
    private string $pathSource;
    private string $pathTarget;
    private string $path;

    /**
     * DivePath constructor.
     * @param string $pathSource
     * @param string $pathTarget
     * @param InterfacePersistenceDocument $PersistenceDocument
     */
    protected function __construct(string $pathSource, string $pathTarget, InterfacePersistenceDocument $PersistenceDocument){
        $this->pathSource = $pathSource;
        $this->pathTarget = $pathTarget;
        $this->PathFiles = [];
        $this->PersistenceDocument = $PersistenceDocument;
        $this->path = $this->pathSource;
    }

    /**
     * @param string $pathSource
     * @param string $pathTarget
     * @param InterfacePersistenceDocument $PersistenceDocument
     * @return static
     */
    public static function buildDivePath(string $pathSource, string $pathTarget, InterfacePersistenceDocument $PersistenceDocument) : self{
        return new self($pathSource, $pathTarget, $PersistenceDocument);
    }

    /**
     * @throws PathNotValid
     */
    public function __invoke(){
        try {
            $this->processPath($this->pathSource)->processFiles();
        }
        catch(\Exception $ex){
            throw $ex;
        }
    }

    /**
     * It Dives directory tree and searches files.
     * @param string $path Main path
     * @return self
     */
    protected function processPath(string $path) : self
    {
        $isPath = is_dir($path);
        if (!$isPath) {
            throw new \Exception("Error. Ruta no válida. [ " . $path . " ]");
        }

        $pointerPath = opendir($path);
        if ($pointerPath) {
            while ($file = readdir($pointerPath)) {
                $isOwnDir = $file == ".";
                $isParentDir = $file == "..";
                $isPath = is_dir($this->pathSource . $file);
                if ($isPath && !$isOwnDir && !$isParentDir) {
                    $this->processPath($path . $file . "/");
                }
                if (!$isPath) {
                    $pF = new stdClass();
                    $pF->path = $path;
                    $pF->file = $file;
                    $this->PathFiles[] = $pF;
                }
            }
            closedir($pointerPath);
        }
        return $this;
    }

    /**
     * Process the file.
     * Get extension, tags and mime.
     * Create a new unique name.
     * It Creates a directory if necessary and copies the file in it.
     */
    private function processFiles() : self
    {
        foreach($this->PathFiles as $item){
            //Extraer la extension y el nombre
            $fileAgent = Files::buildFiles($item->file);
            //Extraer el mime
            $mime = $fileAgent->getMime();
            //Separar para cada nombre las diferentes tags
            $tags = $fileAgent->getTags();
            //Generar índice único
            $newName = $fileAgent->getNewUniqueName();

            //Crear nuevo directorio en caso necesario
            //Copiar o ¿mover? el fichero
            $isCreate = $this->createDirCopyFile($item->path, $item->file, $newName);
            if (!$isCreate) {
                throw new PathNotValid('Error. Don\'t create directory');
            }

            //Registrar en la base de datos
            $docu = Document::factoryFromArray(null,(string)$item->file,$newName, $mime, $tags);
            $per = $this->PersistenceDocument;
            try {
                $per->saveDataToPersistenceSystem($docu);
            } catch (\PDOException $e) {
                throw $e;
            }
        }
        return $this;
    }


    /**
     * Crear un directorio para el fichero nuevo y copiarlo
     * @param string $path
     * @param string $file
     * @param string $newName
     * @return bool
     */
    private function createDirCopyFile(string $path, string $file, string $newName): bool
    {
        $dir = substr($newName, 0, 4);
        $ruta = $this->pathTarget;
        $isDirExist = is_dir($ruta . $dir);
        if (!$isDirExist) {
            $isDir = mkdir($ruta . $dir);
            if (!$isDir) {
                return false;
            }
        }
        copy($path . $file, $ruta . $dir . '/' . $newName);
        return true;
    }
}