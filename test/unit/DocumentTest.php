<?php declare(strict_types=1);

namespace test\unit;

use PHPUnit\Framework\TestCase;
use scan\document\models\ArrayDocument;
use scan\document\models\Document;

final class DocumentTest extends TestCase
{
    public function testFactoryFromArray(): void
    {
        $doc = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $this->assertEquals(
            (string)$doc->getName(),
            (string)"Teoria del todo"
        );


    }

    public function testArray(): void
    {
        $doc0 = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $doc1 = Document::factoryFromArray(null,"Teoria del nada","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $colection = new ArrayDocument();
        $colection->add($doc0);
        $colection->add($doc1);
        
        $res =  (json_decode(json_encode($colection)));

        $this->assertEquals(
            (string)$colection->getListado()[0]->getName(),
            (string)"Teoria del todo"
        );

        $this->assertEquals(
            count($res),
            2
        );

    }

    public function testArrayAlt(): void
    {
        $doc0 = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $doc1 = Document::factoryFromArray(null,"Teoria del nada","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $colection = new ArrayDocument();
        $colection[] = $doc0;
        $colection[] = $doc1;
        
        $res =  (json_decode(json_encode($colection)));

        $this->assertEquals(
            (string)$colection[0]->getName(),
            (string)"Teoria del todo"
        );

        $this->assertEquals(
            count($res),
            2
        );

    }

}