<?php declare(strict_types=1);

namespace test\unit\service;

use PHPUnit\Framework\TestCase;
use scan\services\files\Files;

final class FilesTest extends TestCase
{
    public function testMain(): void
    {
        $file = "teoria java ddd.pdf";
        $ge = Files::buildFiles($file);
        $this->assertEquals((string)"pdf", (string)$ge->getExtension());
        $this->assertEquals((string)"application/pdf", (string)$ge->getMime());
        $this->assertEquals(['teoria','java','ddd'], $ge->getTags());
    }
}