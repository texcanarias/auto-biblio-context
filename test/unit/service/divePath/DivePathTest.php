<?php

declare(strict_types=1);

namespace test\unit\service\divePath;

use PHPUnit\Framework\TestCase;

use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;

final class DivePathTest extends TestCase
{
    private string $pathSource = "./test/test_doc/NEW/";
    private string $pathTarget = "./test/test_doc/ORDER/";
    public function testMain(): void
    {
        try {
            $t = DivePathPublic::buildDivePath(
                                                $this->pathSource,
                                                $this->pathTarget,
                                                new Per()
                                            );
            $res = $t->processPathPublic();
            $this->assertEquals((string)"./test/test_doc/NEW/dir/", (string)$res[1]->path);
            $this->assertEquals((string)"vuejs_cookbook.pdf", (string)$res[1]->file);
        } catch (\Exception $ex) {
            echo "\n";
            echo $ex->getMessage() . "\n";
            echo $ex->getFile() . "\n";
            echo $ex->getLine() . "\n";
        }
    }
}

class Per implements InterfacePersistenceDocument{

    public function saveDataToPersistenceSystem(Document $document): int
    {
        return 0;
    }

    public function getDataFromPersistenceSystem(int $id): ?Document
    {
        return null;
    }

    public function deleteDataFromPersistenceSystem(int $id): Document
    {
        return Document::factoryFromArray(null,'','','',[]);
    }
}