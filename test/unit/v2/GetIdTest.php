<?php declare(strict_types=1);
namespace test\unit\service;

use PHPUnit\Framework\TestCase;
use scan\document\services\GetByIdService;
use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\document\messages\GetByIdMessage;

final class GetIdTest extends TestCase
{
    public function testMain(): void
    {
        $res = GetByIdService::execute(GetByIdMessage::create(5) , new Per());

        $this->assertEquals('scan\document\models\Document', get_class($res) );
        $this->assertEquals('nombre', $res->getName() );        
    }
}

class Per implements InterfacePersistenceDocument
{
    public function saveDataToPersistenceSystem(Document $document) : int{
        return 5;
    }

    public function getDataFromPersistenceSystem(int $id) : ?Document{
        return Document::factoryFromArray(5,'nombre','file.pdf','application/pdf',[]);
    }

    public function deleteDataFromPersistenceSystem(int $id) : Document{
        return Document::factoryFromArray(5,'nombre','file.pdf','application/pdf',[]);
    }
}