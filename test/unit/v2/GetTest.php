<?php declare(strict_types=1);
namespace test\unit\service;

use PHPUnit\Framework\TestCase;
use scan\document\services\GetService;
use scan\document\models\ArrayDocument;
use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceArrayDocument;

final class GetTest extends TestCase
{
    public function testMain(): void
    {
        $res = GetService::execute(new PerArray());

        $this->assertEquals('scan\document\models\ArrayDocument', get_class($res) );
        $this->assertEquals('nombre', $res[0]->getName() );        
    }
}

class PerArray implements InterfacePersistenceArrayDocument
{
    public function getAll() : ArrayDocument{
        $a =  new ArrayDocument();
        $a[] = Document::factoryFromArray(null,'nombre','file.pdf','application/pdf',[]);
        return $a;
    }

    public function getAllFromTag(string $tag) : ArrayDocument{
        return new ArrayDocument();
    }

    public function setPagination(int $page, int $num) : void{
    }

    public function setFilter(\scan\document\persistences\DocumentFilter $documentFilter) : void{        
    }
}