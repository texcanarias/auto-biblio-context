# Gestión automática de los libros y otros documentos (contexto)

## Colocación de los documentos

Los documentos originalmente se meterán en el directorio 'NEW' para
cada documento se generá una nube de tags basada en el nombre.
En base al nombre se tomará los 5 primeros carácteres y se generará (o usará) un directorio con ese nombre. Dentro de la carpeta ORDER.
Cada uno de los tags puede ser anulado.

## Estructura de datos

```plantuml

@startuml
skinparam backgroundColor #EEE

entity "documents"{
    *id: number <<generated>>
    name: string<100>
    name_file: string<255>
    description: blog
}

entity "tags"{
    *id: number <<generated>>
    name: string<100>
}

entity "rel_documents_tags"{
    documents_id: number
    tags_id: number
}

documents ||----|{ rel_documents_tags
tags ||----|{ rel_documents_tags

@enduml
```

## Notas sobre composer

Para verificar que composer está correcto:
- composer validate --no-check-all --strict
